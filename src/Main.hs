module Main where

import Week1.HW01Tests as W1
import Testing

main :: IO ()
main = do
  putStrLn "Run the tests"
  print $ runTests W1.allTests

foo :: IO ()
foo = print $ runTests W1.ex1Tests
